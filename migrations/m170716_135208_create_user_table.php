<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170716_135208_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'password' => $this->string(),
            'auth_Key' => $this->string(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
