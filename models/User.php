<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper; 


class User extends ActiveRecord implements \yii\web\IdentityInterface
{

public $role;

	public static function tableName(){
		return 'user';
	
	}

	public function rules()
	{
	
		return 
		[
			[['username','password','auth_Key','name'],'string','max' => 255],
			[['username','password'],'required'],
			[['username'],'unique'],
			['role', 'safe'],
		];
	}

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user = self::findOne($id);
		return $user;
		//return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Not supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_Key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($auth_Key)
    {
        return $this->auth_Key === $auth_Key;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    
	/** password check default yii
	public function validatePassword($password)
    {
        return $this->password === $password;
    }
	*/
	
	
	//test hashed password

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	
	//hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_Key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	
	//create fullname pseuodo field
//	public function getFullname()
 //   {
  //      return $this->firstname.' '.$this->lastname;
  //  }
	
	//A method to get an array of all users models/User
	public static function getUsers()
	{
		$users = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $users;						
	}
	/*	public static function getRoles()
	{

		$rolesObjects = Yii::$app->authManager->getRoles();
		$roles = [];
		foreach($rolesObjects as $id =>$rolObj){
			$roles[$id] = $rolObj->name; 
		}
		
		return $roles; 		
	}

	public function afterSave($insert,$changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);
		
		$auth = Yii::$app->authManager;
		
		if (\Yii::$app->authManager->getRolesByUser($this->id) == null){
				// auto assign
		}else{
				if(isset($this->role)){
					$role_name = $this->role;
					$role = $auth->getRole($role_name);
					$db = \Yii::$app->db;
					$db->createCommand()->delete('auth_assignment',
						['user_id' => $this->id])->execute();
					$auth->assign($role, $this->id);	
				}	
		}	

        return $return;
    }
*/	
}
